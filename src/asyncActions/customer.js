import {addManyCustomerAction} from "../Store/customerReducer";

export const fetchCustomer=()=>{
  return dispatch =>{
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => dispatch(addManyCustomerAction(json)))

  }
}