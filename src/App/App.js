import React from 'react';
import style from './App.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {addCustomerAction, removeCustomerAction} from '../Store/customerReducer'
import {fetchCustomer} from '../asyncActions/customer'

function App() {

  const dispatch = useDispatch()
  const cash = useSelector(state => state.cash.cash)
  const customer = useSelector(state => state.customer.customer)

  const addCash = (cash) => {
    dispatch({type: 'ADD_CASH', payload: cash})
  }
  const getCash = (cash) => {
    dispatch({type: 'GET_CASH', payload: cash})
  }
  const addCustomer = (name) => {
    const customer={
      id:  Date.now(),
      name: name
    }
    dispatch(addCustomerAction(customer))
  }
  const getCustomer = (id) => {
    dispatch(removeCustomerAction(id))
  }


  return (
    <div className={`${style.wrapper} container`}>
      <h1>{cash}</h1>
      <div className={`${style.wrapper__btn}`}>
        <a onClick={() => addCash(+prompt())} className="waves-effect waves-light btn">Add</a>
        <a onClick={() => getCash(+prompt())} className="waves-effect waves-light btn">Get</a>
      </div>
      <div className={`${style.wrapper__btn}`}>
        <a onClick={() => addCustomer(prompt())} className="waves-effect waves-light btn">Add customer</a>
        <a onClick={() => dispatch(fetchCustomer())} className="waves-effect waves-light btn">Get many customer</a>
      </div>
      {!customer.length ?
        <div>Emty list</div> :
        <ul className={style.customer__list}>{customer.map(e=><li onClick={() => getCustomer(e.id)}className={style.customer__item}>{e.name}</li>)}</ul>
        }
    </div>
  );
}

export default App;
