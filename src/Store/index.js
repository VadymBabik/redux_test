import {createStore, combineReducers, applyMiddleware} from "redux";
import {CashReducer} from "./cashReduser";
import {CustomerReducer} from "./customerReducer";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";

const rootReducer=combineReducers( {
  cash: CashReducer,
  customer: CustomerReducer
})

export const store=createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
